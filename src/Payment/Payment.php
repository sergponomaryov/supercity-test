<?php

namespace Playkot\PhpTestTask\Payment;

final class Payment implements IPayment
{
    /** @var string */
    private $id;
    /** @var \DateTimeImmutable */
    private $created;
    /** @var \DateTimeImmutable */
    private $updated;
    /** @var bool */
    private $isTest;
    /** @var Currency */
    private $currency;
    /** @var float */
    private $amount;
    /** @var float */
    private $taxAmount;
    /** @var State */
    private $state;

    public static function instance(
        string $paymentId,
        \DateTimeInterface $created,
        \DateTimeInterface $updated,
        bool $isTest,
        Currency $currency,
        float $amount,
        float $taxAmount,
        State $state
    ): IPayment {
        if ($paymentId === '') {
            throw new \InvalidArgumentException('Payment ID should not be empty');
        }
        if ($amount < 0) {
            throw new \InvalidArgumentException('Payment amount should be greater than or equal to 0');
        }
        if ($taxAmount < 0) {
            throw new \InvalidArgumentException('Payment tax amount should be greater than or equal to 0');
        }

        $self = new self();
        $self->id = $paymentId;
        $self->created = $created instanceof \DateTime ? \DateTimeImmutable::createFromMutable($created) : $created;
        $self->updated = $updated instanceof \DateTime ? \DateTimeImmutable::createFromMutable($updated) : $updated;
        $self->isTest = $isTest;
        $self->currency = $currency;
        $self->amount = $amount;
        $self->taxAmount = $taxAmount;
        $self->state = $state;

        return $self;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getCreated(): \DateTimeInterface
    {
        return $this->created;
    }

    public function getUpdated(): \DateTimeInterface
    {
        return $this->updated;
    }

    public function isTest(): bool
    {
        return $this->isTest;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getTaxAmount(): float
    {
        return $this->taxAmount;
    }

    public function getState(): State
    {
        return $this->state;
    }

    private function __construct()
    {
    }
}
