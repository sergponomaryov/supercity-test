<?php

namespace Playkot\PhpTestTask\Storage;

use MongoDB\BSON\UTCDateTime;
use MongoDB\Client;
use MongoDB\Collection;
use Playkot\PhpTestTask\Payment\Currency;
use Playkot\PhpTestTask\Payment\IPayment;
use Playkot\PhpTestTask\Payment\Payment;
use Playkot\PhpTestTask\Payment\State;
use Playkot\PhpTestTask\Storage\Exception\NotFound;

/**
 * @todo Extract changeset computation logic + tests
 */
class MongoDBStorage implements IStorage
{
    /** @var Collection */
    private $collection;
    /** @var array */
    private $originalData = [];

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public static function instance(array $config = null): IStorage
    {
        return new self((new Client())->selectCollection('test', 'payments'));
    }

    public function save(IPayment $payment): IStorage
    {
        if ($this->has($payment->getId())) {
            $this->update($payment);
        } else {
            $this->insert($payment);
        }

        return $this;
    }

    public function has(string $paymentId): bool
    {
        return null !== $this->find($paymentId);
    }

    public function get(string $paymentId): IPayment
    {
        $data = $this->find($paymentId);
        if ($data === null) {
            throw new NotFound("Payment '{$paymentId}' not found");
        }

        return Payment::instance(
            $data['_id'],
            $data['created']->toDateTime(),
            $data['updated']->toDateTime(),
            $data['is_test'],
            Currency::get($data['currency']),
            $data['amount'],
            $data['tax_amount'],
            State::get($data['state'])
        );
    }

    public function remove(IPayment $payment): IStorage
    {
        unset($this->originalData[$payment->getId()]);
        $this->collection->deleteOne(['_id' => $payment->getId()]);

        return $this;
    }

    /**
     * @param string $paymentId
     *
     * @return array|null
     */
    private function find(string $paymentId)
    {
        if (isset($this->originalData[$paymentId])) {
            return $this->originalData[$paymentId];
        }

        $data = $this->collection->findOne(['_id' => $paymentId]);
        if ($data !== null) {
            $this->originalData[$paymentId] = (array) $data;
        }

        return $data;
    }

    private function insert(IPayment $payment)
    {
        $paymentDAta = $this->etractPaymentData($payment);
        $this->collection->insertOne($paymentDAta);
    }

    private function update(IPayment $payment)
    {
        $paymentData = $this->etractPaymentData($payment);
        if (isset($this->originalData[$payment->getId()])) {
            $changeset = array_diff($paymentData, $this->originalData[$payment->getId()]);
        } else {
            $changeset = $paymentData;
        }
        unset($changeset['_id']);
        if ($changeset === []) {
            return;
        }

        $this->collection->updateOne(['_id' => $payment->getId()], ['$set' => $changeset]);
        $this->originalData[$payment->getId()] = $paymentData;
    }

    private function etractPaymentData(IPayment $payment): array
    {
        return [
            '_id'        => $payment->getId(),
            'created'    => new UTCDateTime($payment->getCreated()),
            'updated'    => new UTCDateTime($payment->getUpdated()),
            'is_test'    => $payment->isTest(),
            'currency'   => $payment->getCurrency()->getCode(),
            'amount'     => $payment->getAmount(),
            'tax_amount' => $payment->getTaxAmount(),
            'state'      => $payment->getState()->getCode(),
        ];
    }
}
