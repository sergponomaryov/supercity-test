<?php

namespace Playkot\PhpTestTask\Storage;

use Playkot\PhpTestTask\Payment\Currency;
use Playkot\PhpTestTask\Payment\IPayment;
use Playkot\PhpTestTask\Payment\Payment;
use Playkot\PhpTestTask\Payment\State;
use Playkot\PhpTestTask\Storage\Exception\NotFound;
use Predis\Client;

/**
 * @todo Extract changeset computation logic + tests
 */
class PredisStorage implements IStorage
{
    /** @var Client */
    private $client;
    /** @var array */
    private $originalData = [];

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public static function instance(array $config = null): IStorage
    {
        return new self(new Client());
    }

    public function save(IPayment $payment): IStorage
    {
        $paymentData = $this->extractPaymentData($payment);
        if ($this->has($payment->getId())) {
            $changeset = array_diff($paymentData, $this->originalData[$payment->getId()] ?? []);
        } else {
            $changeset = $paymentData;
        }

        $this->client->hmset($payment->getId(), $changeset);
        $this->originalData[$payment->getId()] = $paymentData;

        return $this;
    }

    public function has(string $paymentId): bool
    {
        return [] !== $this->find($paymentId);
    }

    public function get(string $paymentId): IPayment
    {
        $paymentData = $this->find($paymentId);
        if ($paymentData === []) {
            throw new NotFound("Payment '{$paymentId} not found'");
        }

        return Payment::instance(
            $paymentData['id'],
            \DateTimeImmutable::createFromFormat(\DateTime::ATOM, $paymentData['created']),
            \DateTimeImmutable::createFromFormat(\DateTime::ATOM, $paymentData['updated']),
            '1' === $paymentData['is_test'],
            Currency::get($paymentData['currency']),
            (float) $paymentData['amount'],
            (float) $paymentData['tax_amount'],
            State::get((int) $paymentData['state'])
        );
    }

    public function remove(IPayment $payment): IStorage
    {
        $this->client->del([$payment->getId()]);
        unset($this->originalData[$payment->getId()]);

        return $this;
    }

    private function find(string $paymentId): array
    {
        if (isset($this->originalData[$paymentId])) {
            return $this->originalData[$paymentId];
        }

        $data = $this->client->hgetall($paymentId);
        if ($data !== []) {
            $this->originalData[$paymentId] = $data;
        }

        return $data;
    }

    private function extractPaymentData(IPayment $payment): array
    {
        return [
            'id'         => $payment->getId(),
            'created'    => $payment->getCreated()->format(\DateTime::ATOM),
            'updated'    => $payment->getUpdated()->format(\DateTime::ATOM),
            'is_test'    => $payment->isTest() ? '1' : '0',
            'currency'   => $payment->getCurrency()->getCode(),
            'amount'     => (string) $payment->getAmount(),
            'tax_amount' => (string) $payment->getTaxAmount(),
            'state'      => $payment->getState()->getCode(),
        ];
    }
}
